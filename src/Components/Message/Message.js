import React from "react";
import "./Message.css";


const Message = (props) => {
    return (
        <div className="Message">
            <p>Дата:{props.date}</p>
            <p>Автор: {props.author}</p>
            <p>Сообщение: {props.message}</p>
        </div>
    );
};

export default Message;
